import { IProfile } from "common/interface";
import { PROFILE } from "common/querykey";
import { useQueryClient } from "react-query";

export default function useProfile() {
  const queryClient = useQueryClient();
  const profile: IProfile | undefined = queryClient.getQueryData(PROFILE);
  return profile;
}
