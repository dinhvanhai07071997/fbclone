import React from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import "styles/index.scss";
import "helper/i18n";

import App from "./App";

import { ConfigProvider } from "antd";

ReactDOM.render(
  <ConfigProvider>
    <App />
  </ConfigProvider>,
  document.getElementById("root")
);
