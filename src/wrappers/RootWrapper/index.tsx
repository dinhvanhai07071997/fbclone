import Cookies from "js-cookie";
import React, { lazy } from "react";
import { Route, Switch } from "react-router-dom";
import AuthWrapper from "wrappers/AuthWrapper";

const Login = lazy(() => import("pages/Authentication/LogIn"));
const ConfirmEmail = lazy(() => import("pages/Authentication/ConfirmEmail"));

export default function RootWrapper() {
  const loginedIds = Cookies.get("loginedIds");

  return (
    <div className="root-wrapper">
      <Switch>
        <Route path="/login" exact component={Login} />
        <Route path="/confirm-email" exact component={ConfirmEmail} />
        <Route path="/" component={AuthWrapper} />
      </Switch>
    </div>
  );
}
