import { getProfile } from "api/profile";
import { CommonStatus } from "common/enum";
import { IProfile } from "common/interface";
import { PROFILE } from "common/querykey";
import PageHeader from "components/Header";
import Cookies from "js-cookie";
import React, { lazy, Suspense, useEffect, useState } from "react";
import { useQuery } from "react-query";
import OneSignal from "react-onesignal";
import configs from "config";

import { Route, Switch, useHistory } from "react-router-dom";
import styles from "./styles.module.scss";
import { config } from "process";

const Home = lazy(() => import("pages/Home"));
const Friends = lazy(() => import("pages/Friends"));
const Stories = lazy(() => import("pages/Story"));

export default function AuthWrapper() {
  const history = useHistory();
  const [initial, setInitial] = useState(false);

  useEffect(() => {
    OneSignal.init({
      appId: configs.ONE_SIGNAL_APP_ID as string,
    }).then(() => {
      setInitial(true);
      OneSignal.showSlidedownPrompt().then((data: any) => {
        console.log(data);
      });
    });
  }, []);

  useEffect(() => {
    if (initial) {
      OneSignal.addListenerForNotificationOpened((data) => {
        console.log("1", data);
      }).then((data) => {
        console.log("Received NotificationOpened:");
        console.log(data);
      });
    }
  }, [initial]);

  const token = Cookies.get("token");
  const isAuthenticated = !!token;

  const { data: profile }: { data?: IProfile; isLoading: boolean } = useQuery(
    PROFILE,
    () => getProfile().then((res: any) => res.data),
    {
      onSuccess: (data: IProfile) => {
        const loginedIdsString = Cookies.get("loginedIds");
        let ids = loginedIdsString ? JSON.parse(loginedIdsString) : [];

        const arrStr = JSON.stringify([
          ...(new Set([...ids, data?.id]) as any),
        ]);
        Cookies.set("loginedIds", arrStr, { expires: 900000 });
        OneSignal.sendTag("memberId", data?.id);
      },
    }
  );

  if (!isAuthenticated) history.push("/login");

  if (profile?.status === CommonStatus.VERIFY) {
    history.push("/confirm-email");
  }

  return (
    <div className={styles.container}>
      <PageHeader />
      <div className={styles.authWrapper}>
        <Suspense fallback={null}>
          <Switch>
            <Route path="/home" component={Home} />
            <Route path="/friends" component={Friends} />
            <Route path="/stories" component={Stories} />
            <Route path="/stories-create" component={Stories} />
          </Switch>
        </Suspense>
      </div>
    </div>
  );
}
