import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import HttpApi from 'i18next-http-backend';
import vi from 'locales/vi.json';
import en from 'locales/en.json';

i18n
.use(HttpApi)
  .use(initReactI18next)
  .use(LanguageDetector)
  .init({
    supportedLngs: ['en', 'vi'],
    fallbackLng: 'vi',
    debug: false,
  // Options for language detector
    detection: {
    order: ['path', 'cookie', 'htmlTag'],
    caches: ['cookie'],
    },
    interpolation: {
      escapeValue: false,
    },
    resources: {
        vi: { translation: vi },
        en: { translation: en },
      },
  });
