import queryString from "query-string";
import { History } from "history";
import { message } from "antd";
import configs from "config";

export const updateLocationSearchQuery = (history: History, url: any) => {
  let query = queryString.parse(window.location.search);
  //   query = { ...query, ...data };
  //   history.push({
  //     pathname: window.location.pathname,
  //     search: queryString.stringify(query),
  //   });

  history.push(url);
};

export const handleErrorMessage = (error: any) => {
  message.destroy();
  message.error(getErrorMessage(error), 5);
  if (configs.APP_ENV !== "prod") {
    // tslint:disable-next-line: no-console
    console.log(error);
  }
};

export const getErrorMessage = (error: any) => {
  return error?.response?.data?.errorMessage || "Something went wrong";
};
