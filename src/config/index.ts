const configs = {
  APP_ENV: process.env.REACT_APP_ENV,
  API_DOMAIN: process.env.REACT_APP_API_DOMAIN,
  ONE_SIGNAL_APP_ID: process.env.REACT_APP_ONE_SIGNAL_APP_ID,
};

export default configs;
