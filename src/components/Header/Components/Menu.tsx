import React from "react";
import { useTranslation } from "react-i18next";

import styles from "./styles.module.scss";

export const MenuHeader = () => {
  const { t } = useTranslation();
  return (
    <div className={styles.menu}>
      <div className="text-header">{t("common.menu")}</div>

      <div className={styles.menuWrapper}>
        <div className={styles.leftSide}>Left side</div>
        <div className={styles.rightSide}>
          <div className="text-header-2">{t("common.create")}</div>

          <div>
            
          </div>
        </div>
      </div>
    </div>
  );
};
