import { Carousel, Divider } from "antd";
import { logout } from "api/axios";
import { IMAGE } from "common/const";
import useProfile from "hook/useProfile";
import React, { useRef } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import styles from "./styles.module.scss";

export const Account = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const profile = useProfile();

  const slider: any = useRef(null);

  const handleLogout = () => {
    logout();
    history.push("/login");
  };
  return (
    <div className={styles.settingWrapper}>
      <Carousel
        ref={slider}
        infinite={false}
        dots={false}
        speed={100}
        adaptiveHeight={true}
      >
        <div>
          <div
            className={styles.switchAccount}
            onClick={() => slider.current.next()}
          >
            <div className="d-flex align-items-center">
              <img src={profile?.avatar || IMAGE} alt="" />
              <span>{t("common.switchAccount")}</span>
            </div>
            <div>
              <i
                data-visualcompletion="css-img"
                className={styles.iconNext}
              ></i>
            </div>
          </div>

          <div>
            <div className={styles.logOut} onClick={handleLogout}>
              <div className={styles.iconWrapper}>
                <i
                  data-visualcompletion="css-img"
                  className={styles.iconLogout}
                ></i>
              </div>
              <span>{t("common.logOut")}</span>
            </div>
          </div>
          <div className="mt-10 text-right">Hai dinh social facebook @2022</div>
        </div>

        <div onClick={() => slider.current.prev()}>
          <div className={styles.arrowHeader}>
            <div
              className={styles.arrowButton}
              onClick={() => slider.current.prev()}
            >
              <i
                data-visualcompletion="css-img"
                className={styles.arrowRight}
              ></i>
            </div>
            <span>{t("common.switchAccount")}</span>
          </div>

          <div className={styles.text}>{t("setting.switchAccountContent")}</div>

          <div className={styles.listAccount}>
            <div className={styles.accountItem}>
              <div className={styles.accountInfo}>
                <div className={styles.image}>
                  <img src={IMAGE} alt="" />
                </div>
                <div className={styles.name}>Jessica</div>
              </div>
              <div className={styles.xIcon}>
                <i data-visualcompletion="css-img" className={styles.icon}></i>
              </div>
            </div>
          </div>

          <Divider />

          <div className={styles.loginOther}>
            <div className={styles.iconAdd}>
              <i data-visualcompletion="css-img"></i>
            </div>

            <div className={styles.loginText}>
              {t("setting.loginOtherAccount")}
            </div>
          </div>
        </div>
      </Carousel>
    </div>
  );
};
