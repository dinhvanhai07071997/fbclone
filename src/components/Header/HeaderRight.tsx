import { Tooltip } from "antd";
import icArrowDown from "assets/icons/arrow-down.svg";
import icNotiGreen from "assets/icons/bell-green.svg";
import icNoti from "assets/icons/bell.svg";
import icArrowDownGreen from "assets/icons/down-green.svg";
import icMenu from "assets/icons/menu-dot.svg";
import icMenuGreen from "assets/icons/menu-green.svg";
import icMessageGreen from "assets/icons/message-green.svg";
import icMessage from "assets/icons/messenger.svg";
import { Account } from "components/Header/Components/Account";
import { ListMessage } from "components/Header/Components/ListMessage";
import { ListNoti } from "components/Header/Components/ListNoti";
import { MenuHeader } from "components/Header/Components/Menu";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import styles from "./styles.module.scss";

type ActiveElement = "menu" | "message" | "noti" | "setting" | "";
export default function HeaderRight() {
  const { t } = useTranslation();
  const [activeElement, setActiveElement] = useState<ActiveElement>("");
  const [open, setOpen] = useState(false);
  const renderComponent = () => {
    switch (activeElement) {
      case "menu":
        return <MenuHeader />;
      case "message":
        return <ListMessage />;
      case "noti":
        return <ListNoti />;
      case "setting":
        return <Account />;
      default:
        return "";
    }
  };

  const onClick = (key: ActiveElement) => {
    if (activeElement === key) {
      setOpen(false);
      setActiveElement("");
    } else {
      setOpen(true);
      setActiveElement(key);
    }
  };
  return (
    <>
      <Tooltip title={t("common.menu")} zIndex={1}>
        <div className={styles.roundedButton} onClick={() => onClick("menu")}>
          <img src={activeElement === "menu" ? icMenuGreen : icMenu} alt="" />
        </div>
      </Tooltip>

      <Tooltip title={t("common.messenger")} zIndex={1}>
        <div
          className={styles.roundedButton}
          onClick={() => onClick("message")}
        >
          <img
            src={activeElement === "message" ? icMessageGreen : icMessage}
            alt=""
          />
        </div>
      </Tooltip>

      <Tooltip title={t("common.notifications")} zIndex={1}>
        <div className={styles.roundedButton} onClick={() => onClick("noti")}>
          <img src={activeElement === "noti" ? icNotiGreen : icNoti} alt="" />
        </div>
      </Tooltip>

      <Tooltip title={t("common.yourProfile")} zIndex={1}>
        <div
          className={styles.roundedButton}
          onClick={() => onClick("setting")}
        >
          <img
            src={activeElement === "setting" ? icArrowDownGreen : icArrowDown}
            alt=""
          />
        </div>
      </Tooltip>

      {open && renderComponent()}
    </>
  );
}
