import { Col, Menu, Row, Tooltip } from "antd";
import icFriendFilled from "assets/icons/friends-filled.svg";
import icFriend from "assets/icons/friends.svg";
import icGameCenterFilled from "assets/icons/game-center-filled.svg";
import icGameCenter from "assets/icons/game-center.svg";
import icHomeFilled from "assets/icons/home-filled.svg";
import icHome from "assets/icons/home.svg";
import icLogo from "assets/icons/logo.svg";
import icMarketFilled from "assets/icons/maket-place-filled.svg";
import icMarket from "assets/icons/market.svg";
import icSearch from "assets/icons/search.svg";
import icWatchFilled from "assets/icons/watch-filled.svg";
import icWatch from "assets/icons/watch.svg";
import { IMAGE } from "common/const";
import useProfile from "hook/useProfile";
import React from "react";
import { useTranslation } from "react-i18next";
import { useHistory, useLocation } from "react-router-dom";
import HeaderRight from "./HeaderRight";
import styles from "./styles.module.scss";

export default function PageHeader() {
  const { t } = useTranslation();
  const history = useHistory();
  const profile = useProfile();
  const { pathname } = useLocation();

  const routes = [
    {
      key: "home",
      title: t("common.home"),
      url: "/",
      icon: pathname === "/" ? icHomeFilled : icHome,
    },
    {
      key: "friends",
      title: t("common.friends"),
      url: "/friends",
      icon: pathname === "/friends" ? icFriendFilled : icFriend,
    },
    {
      key: "watches",
      title: t("common.watch"),
      url: "/watch",
      icon: pathname === "/watch" ? icWatchFilled : icWatch,
    },
    {
      key: "market",
      title: t("common.marketPlace"),
      url: "/market-place",
      icon: pathname === "/market-place" ? icMarketFilled : icMarket,
    },
    {
      key: "game-center",
      title: t("common.gaming"),
      url: "/game-center",
      icon: pathname === "/game-center" ? icGameCenterFilled : icGameCenter,
    },
  ];
  return (
    <div className={styles.headerContainer}>
      <Row gutter={0}>
        <Col className="d-flex align-items-center pl-15" span={6}>
          <div>
            <img src={icLogo} className={styles.logo} alt="" />
          </div>
          <div>&nbsp;&nbsp;</div>
          <div className={styles.searchBox}>
            <img src={icSearch} alt="" />
            <div>{t("header.search_message")}</div>
          </div>
        </Col>

        <Col span={11} className={styles.menu}>
          <Menu mode="horizontal" defaultSelectedKeys={[]}>
            {routes.map((route) => (
              <Menu.Item key={route.key}>
                <Tooltip title={route.title}>
                  <div onClick={() => history.push(route.url)}>
                    <img src={route.icon} alt="" />
                  </div>
                </Tooltip>
              </Menu.Item>
            ))}
          </Menu>
        </Col>

        <Col span={7} className={styles.rightHeader}>
          <div className={styles.avatarProfile}>
            <img src={profile?.avatar || IMAGE} alt="" />
            {profile?.firstName}
          </div>
          <HeaderRight />
        </Col>
      </Row>
    </div>
  );
}
