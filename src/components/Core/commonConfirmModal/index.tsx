import { Button, ButtonProps, Modal, ModalProps } from "antd";
import React, { Children, ReactNode } from "react";
import styles from "./styles.module.scss";

interface IPropsModal extends ModalProps {
  children: ReactNode;
}

export const CommonConfirmModal = ({
  onOk,
  onCancel,
  title,
  visible,
  children,
  okText,
  cancelText,
  maskClosable,
  ...rest
}: IPropsModal) => {
  return (
    <Modal
      title={title}
      visible={visible}
      onOk={onOk}
      onCancel={onCancel}
      centered={true}
      okText={okText}
      maskClosable={maskClosable}
      cancelText={cancelText}
      {...rest}
      width={548}
      className={styles.modal}
    >
      {children}
    </Modal>
  );
};
