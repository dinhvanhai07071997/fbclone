import { CloseOutlined } from "@ant-design/icons";
import { Modal, ModalProps } from "antd";
import React, { ReactNode } from "react";
import styles from "./styles.module.scss";

interface IPropsModal extends ModalProps {
  children: ReactNode;
}

export const CommonModal = ({
  title,
  visible,
  children,
  okText,
  cancelText,
  maskClosable,
  ...rest
}: IPropsModal) => {
  return (
    <Modal
      title={title}
      visible={visible}
      centered={true}
      okText={okText}
      maskClosable={maskClosable}
      cancelText={cancelText}
      {...rest}
      width={548}
      className={styles.modal}
    >
      {children}
    </Modal>
  );
};
