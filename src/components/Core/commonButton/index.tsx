import { Button, ButtonProps } from "antd";
import React from "react";
import styles from "./styles.module.scss";

interface IPropsButton extends ButtonProps {
  onClick?: (data?: any) => any;
  title?: any;
  type?: "default" | "primary";
}

export const CommonButton = ({
  onClick,
  title,
  type,
  icon,
  ...rest
}: IPropsButton) => {
  return (
    <Button onClick={onClick} type={type} className={styles.button} {...rest}>
      {title}
    </Button>
  );
};
