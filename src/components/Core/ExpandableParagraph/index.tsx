import React from "react";
import { Typography } from "antd";
const { Paragraph, Text } = Typography;

interface IProps {
  content: string;
}
export default function ExpandableParaGraph({ content }: IProps) {
  return (
    <Paragraph ellipsis={{ rows: 5, expandable: true, symbol: "more" }}>
      {content}
    </Paragraph>
  );
}
