import React, { useState, useEffect } from "react";
import { Progress } from "antd";
import styles from "./styles.module.scss";

const DURATION = 1;
export default function CommonProgressBar() {
  const [percent, setPercent] = useState(0);

  useEffect(() => {
    if (percent < 100) {
      const timer = setTimeout(
        () => setPercent(percent + (100 * 0.05) / DURATION),
        50
      );
      return () => clearTimeout(timer);
    }
  }, [percent]);

  return (
    <>
      <Progress percent={percent} />
    </>
  );
}
