import { placeholder } from "@babel/types";
import { Button, ButtonProps, Input, InputProps } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import React from "react";
import classnames from "classnames";
import styles from "./styles.module.scss";

interface IPropsInput extends InputProps {
  type: "input" | "search" | "password" | "number" | "text-area";
  placeholder: string;
  inputSize?: "small" | "medium" | "big";
}

export const CommonInput = ({
  type = "input",
  inputSize = "medium",
  ...rest
}: IPropsInput) => {
  if (type === "password")
    return (
      <Input.Password
        className={classnames({
          [styles.smallInput]: inputSize === "small",
          [styles.mediumInput]: inputSize === "medium",
          [styles.bigInput]: inputSize === "big",
        })}
        iconRender={(visible) =>
          visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
        }
        {...rest}
      />
    );
  if (type === "text-area") return <Input.TextArea className={styles.input} />;
  return (
    <Input
      className={classnames({
        [styles.smallInput]: inputSize === "small",
        [styles.mediumInput]: inputSize === "medium",
        [styles.bigInput]: inputSize === "big",
      })}
      {...rest}
    />
  );
};
