import { Col, Divider, Row, Tooltip } from "antd";
import React, { useState } from "react";
import { AnimatePresence, motion } from "framer-motion";
import icLike from "assets/icons/like.png";
import icLove from "assets/icons/love.png";
import icCare from "assets/icons/care.png";

import icWow from "assets/icons/wow.png";
import icSad from "assets/icons/sad.png";

import animateSad from "assets/icons/sad.gif";
import animateLove from "assets/icons/love.gif";
import animateHaha from "assets/icons/laugh.gif";
import aniMateCare from "assets/icons/care.gif";
import animateWow from "assets/icons/wow.gif";
import animateAngry from "assets/icons/angry.gif";
import animateLike from "assets/icons/like.gif";
import styles from "./styles.module.scss";

interface IProps {
  data: any;
}

const EMOJI = [
  {
    id: 1,
    img: animateLike,
  },
  {
    id: 2,
    img: animateLove,
  },
  {
    id: 3,
    img: aniMateCare,
  },
  {
    id: 4,
    img: animateHaha,
  },
  {
    id: 5,
    img: animateWow,
  },
  {
    id: 6,
    img: animateSad,
  },
  {
    id: 7,
    img: animateAngry,
  },
];
export default function ReactionSection({ data }: IProps) {
  const [openLike, setOpenLike] = useState(false);

  const handleOpenLike = () => {
    setOpenLike(true);
  };

  const handleCloseLike = () => {
    setOpenLike(false);
  };
  return (
    <>
      <div className={styles.reactionWrapper}>
        <div className={styles.reactionLeft}>
          <Tooltip title="prompt text">
            <img src={icLike} alt="" className={styles.imgReaction} />
          </Tooltip>

          <Tooltip title="prompt text">
            <img src={icLove} alt="" className={styles.imgReaction} />
          </Tooltip>

          <Tooltip title="prompt text">
            <img src={icCare} alt="" className={styles.imgReaction} />
          </Tooltip>
          <Tooltip title="prompt text">
            <img src={icSad} alt="" className={styles.imgReaction} />
          </Tooltip>
          <Tooltip title="prompt text">
            <img src={icWow} alt="" className={styles.imgReaction} />
          </Tooltip>

          <span>1k</span>
        </div>

        <div className={styles.reactionRight}>
          <div>300 comments</div>
          <div>30 shares</div>
        </div>
      </div>

      <Divider className="mt-5 mb-5" />

      <div className={styles.likeWrapper}>
        <Row gutter={10}>
          <Col span={8}>
            <div
              className={styles.sectionLikeWrapper}
              onMouseOver={handleOpenLike}
              onMouseOut={handleCloseLike}
            >
              <span>
                <i
                  data-visualcompletion="css-img"
                  className={styles.likeImg}
                ></i>
                Like
              </span>
            </div>
          </Col>

          <Col span={8}>
            <div className={styles.sectionLikeWrapper}>
              <i
                data-visualcompletion="css-img"
                className={styles.commentImg}
              ></i>
              Comment
            </div>
          </Col>

          <Col span={8}>
            <div className={styles.sectionLikeWrapper}>
              <i
                data-visualcompletion="css-img"
                className={styles.shareImg}
              ></i>
              Share
            </div>
          </Col>
        </Row>

        <AnimatePresence initial={false}>
          {openLike && (
            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              className={styles.reactionEmoji}
              onMouseOver={handleOpenLike}
              onMouseOut={handleCloseLike}
            >
              {EMOJI.map((el) => (
                <motion.div key={el.id} className={styles.reactionEmojiItem}>
                  <motion.img
                    src={el.img}
                    alt=""
                    className={styles.reactionEmojiIcon}
                  />
                </motion.div>
              ))}
            </motion.div>
          )}
        </AnimatePresence>
      </div>
    </>
  );
}
