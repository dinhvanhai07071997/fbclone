import { sendPost } from "./axios";

export const login = (params: any) => sendPost("/auth/login", params);
export const register = (params: any) => sendPost("/auth/register", params);
export const confirmEmail = (params: any) =>
  sendPost("/auth/confirm-email", params);
