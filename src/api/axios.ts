import { message } from "antd";
import Axios from "axios";
import { handleErrorMessage } from "helper";
import Cookies from "js-cookie";
import { history, queryClient } from "../App";
import configs from "../config";

const axiosInstance = Axios.create({
  timeout: 3 * 60 * 1000,
  baseURL: configs.API_DOMAIN,
});

const THIRDTY_DAY = 30;

axiosInstance.interceptors.request.use(
  (config: any) => {
    config.headers.Authorization = `Bearer ${Cookies.get("token")}`;
    return config;
  },
  (error) => Promise.reject(error)
);

export const logout = (error?: any) => {
  Cookies.remove("token");
  Cookies.remove("refreshToken");
  queryClient.clear();

  return Promise.reject(error);
};
axiosInstance.interceptors.response.use(
  (response) => response,
  async (error) => {
    const { data } = error?.response;

    const originalConfig = error?.config;
    if (error?.response?.status !== 401) {
      return Promise.reject(error);
    }

    const refreshToken = Cookies.get("refreshToken");

    if (!refreshToken) {
      logout(error);
      return Promise.reject(error);
    }
    try {
      const res: any = await Axios.post(
        `${configs.API_DOMAIN}/auth/request-access-token`,
        {
          refreshToken,
        }
      );

      if (res.status === 201) {
        const data = res?.data?.data;
        Cookies.set("token", data?.token, { expires: THIRDTY_DAY });
        originalConfig.headers.Authorization = `Bearer ${data?.token}`;
        return Axios(originalConfig);
      } else {
        logout(error);
        return Promise.reject(error);
      }
    } catch (error) {
      logout(error);
      return Promise.reject(error);
    }
  }
);

export const sendGet = (url: string, params?: any) =>
  axiosInstance.get(url, { params }).then((res) => res?.data);
export const sendPost = (url: string, params?: any, queryParams?: any) =>
  axiosInstance
    .post(url, params, { params: queryParams })
    .then((res) => res?.data);
export const sendPut = (url: string, params?: any) =>
  axiosInstance.put(url, params).then((res) => res?.data);
export const sendPatch = (url: string, params?: any) =>
  axiosInstance.patch(url, params).then((res) => res?.data);
export const sendDelete = (url: string, params?: any) =>
  axiosInstance.delete(url, { params }).then((res) => res?.data);
