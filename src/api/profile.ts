import { sendGet, sendPut } from "./axios";

export const getProfile = () => sendGet("/profile");
export const updateProfile = (params: any) => sendPut("/profile", params);
