import React, { Suspense } from "react";
import { createBrowserHistory } from "history";
import RootWrapper from "./wrappers/RootWrapper";
import { QueryClientProvider, QueryClient } from "react-query";
import { Router } from "react-router-dom";

export const history = createBrowserHistory();

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      cacheTime: 24 * 3600 * 1000, // cache for 1 day
      retry: false,
    },
  },
});

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Router history={history}>
        <Suspense fallback={null}>
          <RootWrapper />
        </Suspense>
      </Router>
    </QueryClientProvider>
  );
}

export default App;
