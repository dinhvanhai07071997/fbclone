import { Button, Col, Form, Radio, Row, Select, Tooltip } from "antd";
import { useForm } from "antd/lib/form/Form";
import {
  listDayOfMonth,
  listMonthOfYear,
  listYear,
  PATTERN_EMAIL,
} from "common/const";
import { CommonInput } from "components/Core/commonInput";
import { CommonModal } from "components/Core/commonModal";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import styles from "./styles.module.scss";
import { UserGender } from "common/enum";
import { handleErrorMessage } from "helper";
import { register } from "api/auth";
import Cookies from "js-cookie";
import { useHistory } from "react-router-dom";

const { Option } = Select;

interface IProps {
  handleCancel: () => void;
}

interface IFormvalues {
  firstName: string;
  lastName: string;
  email: string;
  day: number;
  month: number;
  year: number;
  gender: UserGender;
  password: string;
}
export const ModalSignUp = ({ handleCancel }: IProps) => {
  const { t } = useTranslation();
  const [form] = useForm();
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const Title = () => (
    <>
      <div className={styles.title}>{t("signUp.title")}</div>
      <div className={styles.subTitle}>{t("signUp.sub_title")}</div>
    </>
  );

  const handleSubmit = async (values: IFormvalues) => {
    setLoading(true);

    const params = {
      firstName: values.firstName,
      lastName: values.lastName,
      email: values.email,
      dob: `${values.year}-${values.month}-${values.day}`,
      gender: values.gender,
      password: values.password,
    };

    try {
      const data = await register(params);
      Cookies.set("token", data?.data?.token, { expires: 700 });
      Cookies.set("refreshToken", data?.data?.refreshToken, { expires: 700 });
      history.push("/");
    } catch (error) {
      handleErrorMessage(error);
    }
  };

  return (
    <CommonModal
      visible={true}
      onCancel={handleCancel}
      title={<Title />}
      footer={false}
      centered={true}
    >
      <Form form={form} onFinish={handleSubmit}>
        <Row gutter={10}>
          <Col span={12}>
            <Form.Item
              name="firstName"
              rules={[
                {
                  required: true,
                  message: t("common.requireField"),
                },
              ]}
            >
              <CommonInput
                type={"input"}
                inputSize="small"
                placeholder={t("signUp.first_name")}
              />
            </Form.Item>
          </Col>

          <Col span={12}>
            <Form.Item
              name="lastName"
              rules={[
                {
                  required: true,
                  message: t("common.requireField"),
                },
              ]}
            >
              <CommonInput
                type={"input"}
                inputSize="small"
                placeholder={t("signUp.surname")}
              />
            </Form.Item>
          </Col>

          <Col span={24}>
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: t("common.requireField"),
                },
                {
                  pattern: PATTERN_EMAIL,
                  message: t("common.validateEmail"),
                },
              ]}
            >
              <CommonInput
                type={"input"}
                inputSize="small"
                placeholder={t("signUp.email")}
              />
            </Form.Item>
          </Col>

          <Col span={24}>
            <Form.Item
              name="password"
              hasFeedback
              rules={[
                {
                  required: true,
                  message: t("common.requireField"),
                },
              ]}
            >
              <CommonInput
                type={"password"}
                inputSize="small"
                placeholder={t("signUp.new_password")}
              />
            </Form.Item>
          </Col>

          <Form.Item noStyle shouldUpdate={true}>
            {({ getFieldValue }) => {
              const isChangeInputPassword = getFieldValue("password");
              return isChangeInputPassword ? (
                <Col span={24}>
                  <Form.Item
                    name="confirmPassword"
                    hasFeedback
                    rules={[
                      {
                        required: true,
                        message: t("common.requireField"),
                      },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (!value || getFieldValue("password") === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            new Error(
                              "The two passwords that you entered do not match!"
                            )
                          );
                        },
                      }),
                    ]}
                  >
                    <CommonInput
                      type={"password"}
                      inputSize="small"
                      placeholder={t("signUp.confirmPassword")}
                    />
                  </Form.Item>
                </Col>
              ) : null;
            }}
          </Form.Item>

          <Col span={24} className="d-flex align-items-center">
            {t("common.birth_day")}
            <Tooltip
              overlay={t("signUp.birthDayInfo")}
              trigger={"click"}
              overlayInnerStyle={{ backgroundColor: "white", color: "black" }}
            >
              <i className={styles.questionMark}></i>
            </Tooltip>
          </Col>
          <Col span={8}>
            <Form.Item
              name="day"
              rules={[
                {
                  required: true,
                  message: t("common.requireField"),
                },
              ]}
            >
              <Select className={styles.select} placeholder={t("common.day")}>
                {listDayOfMonth.map((el) => (
                  <Option value={el} key={el}>
                    {el}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item
              name="month"
              rules={[
                {
                  required: true,
                  message: t("common.requireField"),
                },
              ]}
            >
              <Select className={styles.select} placeholder={t("common.month")}>
                {listMonthOfYear.map((el) => (
                  <Option value={el} key={el}>
                    {el}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item
              name="year"
              rules={[
                {
                  required: true,
                  message: t("common.requireField"),
                },
              ]}
            >
              <Select className={styles.select} placeholder={t("common.year")}>
                {listYear.map((el) => (
                  <Option value={el} key={el}>
                    {el}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>

          <Col span={24} className="dlex align-items-center">
            {t("common.gender")}
            <Tooltip
              overlay={t("signUp.genderInfo")}
              trigger={"click"}
              overlayInnerStyle={{ backgroundColor: "white", color: "black" }}
            >
              <i className={styles.questionMark}></i>
            </Tooltip>
          </Col>

          <Col span={24}>
            <Form.Item
              name="gender"
              rules={[
                {
                  required: true,
                  message: t("common.requireField"),
                },
              ]}
            >
              <Radio.Group className={styles.radioGroup}>
                <div className={styles.radioGroupItem}>
                  <Radio value={UserGender.MALE}>
                    <div> {t("common.male")}</div>
                  </Radio>
                </div>
                <div className={styles.radioGroupItem}>
                  <Radio value={UserGender.FEMALE}>
                    <div> {t("common.female")}</div>
                  </Radio>
                </div>
                <div className={styles.radioGroupItem}>
                  <Radio value={UserGender.OTHER}>
                    <div> {t("common.other")}</div>
                  </Radio>
                </div>
              </Radio.Group>
            </Form.Item>
          </Col>

          <Col span={24}>
            <div>{t("signUp.term_policy")}</div>
          </Col>
        </Row>

        <Row>
          <Col span={12} offset={6}>
            <Form.Item>
              <Button
                className={styles.signUpButton}
                htmlType="submit"
                type="default"
                loading={loading}
              >
                {t("common.sign_up")}
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </CommonModal>
  );
};
