import { Button, Divider, Dropdown, Form, Menu } from "antd";
import { useForm } from "antd/lib/form/Form";
import { login } from "api/auth";
import logo from "assets/icons/logobig.svg";
import { languages, PATTERN_EMAIL } from "common/const";
import { CommonButton } from "components/Core/commonButton";
import { CommonInput } from "components/Core/commonInput";
import { handleErrorMessage } from "helper";
import i18next from "i18next";
import Cookies from "js-cookie";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { ModalSignUp } from "./ModalSignUp";

import styles from "./styles.module.scss";

export default function Login() {
  const { t } = useTranslation();
  const history = useHistory();

  const [open, setOpen] = useState(false);

  const [isLoading, setIsLoading] = useState(false);
  const [form] = useForm();

  const menu = (
    <Menu onClick={(e) => i18next.changeLanguage(e.key)}>
      {languages.map((el: any, index: number) => (
        <Menu.Item key={el?.code}>{el?.name}</Menu.Item>
      ))}
    </Menu>
  );

  const handleSubmit = async (values: any) => {
    setIsLoading(true);
    let params: any = {
      password: values.password,
    };

    if (values.email.match(PATTERN_EMAIL)) {
      params.email = values.email;
    } else {
      params.phone = values.email;
    }

    try {
      const data = await login(params);

      const { accessToken, refreshToken } = data?.data;
      Cookies.set("token", accessToken, { expires: 700 });
      Cookies.set("refreshToken", refreshToken, { expires: 700 });

      history.push("/home");
    } catch (error) {
      handleErrorMessage(error);
    } finally {
      setIsLoading(false);
    }
  };

  const handleCancel = () => {
    setOpen(false);
    form.resetFields();
  };

  return (
    <div className={styles.pageWrapper}>
      <div className={styles.selectLanguage}>
        <Dropdown overlay={menu} trigger={["hover"]}>
          <Button>Change language</Button>
        </Dropdown>
      </div>

      <div className={styles.container}>
        <div className={styles.mainContent}>
          <div className={styles.contentLeft}>
            <img src={logo} alt="" />
            <h2>{t("login.fb_describe")}</h2>
          </div>
          <div className="">
            <div className={`${styles.contentRight} box-shadow`}>
              <Form form={form} onFinish={handleSubmit}>
                <Form.Item
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: "This field is required",
                    },
                    {
                      pattern: PATTERN_EMAIL,
                      message: "Không đúng định dạng",
                    },
                  ]}
                >
                  <CommonInput type={"input"} placeholder={t("login.email")} />
                </Form.Item>
                <Form.Item
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: "This field is required",
                    },
                  ]}
                >
                  <CommonInput
                    type={"password"}
                    placeholder={t("login.password")}
                  />
                </Form.Item>
                <Form.Item>
                  <CommonButton
                    type="primary"
                    title={t("login.login")}
                    htmlType="submit"
                    loading={isLoading}
                  />
                </Form.Item>
                <div className="d-flex justify-content-center font-size-17">
                  <a href="/forgot-password">{t("login.forgotten_password")}</a>
                </div>
                <Divider />

                <div className="d-flex justify-content-center">
                  <Button
                    className={styles.loginButton}
                    type="default"
                    onClick={() => setOpen(true)}
                  >
                    {t("login.create_new_account")}
                  </Button>
                </div>
              </Form>
            </div>
            <span className="font-size-17 font-weight-bold mt-10">
              {t("login.create_a_page")} &nbsp;
            </span>
            <span className="font-size-17  mt-10">{t("login.footer")}</span>
          </div>
        </div>
      </div>

      {open && <ModalSignUp handleCancel={handleCancel} />}
    </div>
  );
}
