import React, { useEffect, useState } from "react";
import icLogo from "assets/icons/logo.svg";
import icArrowDownGreen from "assets/icons/down-green.svg";
import icArrowDown from "assets/icons/arrow-down.svg";
import styles from "./styles.module.scss";
import { Button, Divider, Form, Tooltip } from "antd";
import { useTranslation } from "react-i18next";
import { Setting } from "./Setting";
import useProfile from "hook/useProfile";
import { CommonInput } from "components/Core/commonInput";
import { CommonButton } from "components/Core/commonButton";
import { useForm } from "antd/lib/form/Form";
import { useMutation, useQueryClient } from "react-query";
import { confirmEmail } from "api/auth";
import { handleErrorMessage } from "helper";
import { PROFILE } from "common/querykey";
import { useHistory } from "react-router-dom";
import { CommonStatus } from "common/enum";

export default function ConfirmEmail() {
  const { t } = useTranslation();
  const profile = useProfile();
  const history = useHistory();
  const queryClient = useQueryClient();
  const [active, setIsActive] = useState(false);
  const [form] = useForm();

  const onClick = () => {
    setIsActive(!active);
  };

  useEffect(() => {
    if (profile?.status === CommonStatus.ACTIVE) history.replace("/home");
  }, [profile]);

  const { mutate, isLoading } = useMutation(confirmEmail);

  const handleSubmit = (values: any) => {
    const params = {
      email: profile?.email,
      verifyCode: values.code,
    };
    mutate(params, {
      onSuccess: async () => {
        await queryClient.invalidateQueries(PROFILE);
        history.replace("/home");
      },
      onError: (error) => {
        handleErrorMessage(error);
      },
    });
  };

  return (
    <div className={styles.wrapper}>
      <div className={styles.headerContainer}>
        <img src={icLogo} className={styles.logo} alt="" />
        <div>
          <Tooltip title={t("common.yourProfile")} zIndex={1}>
            <div className={styles.roundedButton} onClick={onClick}>
              <img src={active ? icArrowDownGreen : icArrowDown} alt="" />
            </div>
          </Tooltip>

          {active && <Setting />}
        </div>
      </div>

      <Form form={form} onFinish={handleSubmit}>
        <div className={styles.formWrapper}>
          <div className={styles.form}>
            <div className={styles.title}>
              <h2>{t("confirmEmail.title")}</h2>
            </div>
            <div className={styles.content}>
              {t("confirmEmail.content")} <b>{profile?.email}</b>{" "}
            </div>

            <div className={styles.code}>
              <div className="mr-10">FB</div>
              <Form.Item name="code">
                <div className={styles.input}>
                  <CommonInput
                    type="input"
                    inputSize="small"
                    placeholder={""}
                  />
                </div>
              </Form.Item>
            </div>

            <Divider />

            <div className="d-flex justify-content-center">
              <Form.Item noStyle shouldUpdate>
                {({ getFieldValue }) => {
                  return (
                    <Form.Item noStyle>
                      <Button
                        className={styles.signUpButton}
                        htmlType="submit"
                        type="default"
                        disabled={!getFieldValue("code")}
                        loading={isLoading}
                      >
                        {t("common.confirm")}
                      </Button>
                    </Form.Item>
                  );
                }}
              </Form.Item>
            </div>
          </div>
        </div>
      </Form>
    </div>
  );
}
