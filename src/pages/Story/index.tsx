import { PlusOutlined } from "@ant-design/icons";
import { Divider, Typography } from "antd";
import icClose from "assets/icons/close.svg";
import icLogo from "assets/icons/logo.svg";
import HeaderRight from "components/Header/HeaderRight";
import querystring from "query-string";
import React from "react";
import { useLocation, useHistory } from "react-router-dom";
import styles from "./styles.module.scss";

const { Link } = Typography;

export default function Story() {
  const location = useLocation();
  const queryString = querystring.parse(location.search);

  const renderStoryView = () => {
    switch (queryString.source) {
      case "story-home":
        return <StoryHome />;

      default:
        return <MainPage />;
    }
  };

  return renderStoryView();
}

const MainPage = () => {
  return (
    <div className={styles.pageWrapper}>
      <div className={styles.leftSide}>
        <div>Tin</div>
      </div>
      <div className={styles.storyWrapper}>
        Chua thong tin cua toan bo story
      </div>
    </div>
  );
};
const StoryHome = () => {
  const history = useHistory();
  const onClose = () => {};
  return (
    <div className={styles.storyDetail}>
      <div className={styles.storyDetailLeft}>
        <div
          className={styles.storyDetailLeftHeader}
          onClick={() => history.push("/")}
        >
          <div className={styles.closeIcon}>
            <img src={icClose} alt="" />
          </div>

          <img src={icLogo} className="cursor-pointer" alt="" />
        </div>

        <Divider className="mt-5 mb-5" />

        <div className={styles.wrapper}>
          <div className={styles.addStoryWrapper}>
            <div className={styles.title}>Tin</div>
            <div className={styles.link}>
              <Link>Kho lưu trữ</Link>
              <span className="mr-5 ml-5">&#183;</span>
              <Link>Cài đặt</Link>
            </div>
            <div className={styles.myStory}>Tin của bạn</div>
            <div className={styles.createStory}>
              <div className={styles.createStoryImage}>
                <PlusOutlined className={styles.icons} />
              </div>
              <div className={styles.createStoryText}>
                <div>Tạo tin </div>
                <p>Bạn có thể chia sẻ ảnh hoặc viết gì đó</p>
              </div>
            </div>
          </div>

          <div className={styles.storyList}>
            <div className={styles.text}>Tất cả tin</div>
            <StoryItem
              avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
              name="Haireus"
              time="3 giờ"
              newItem={2}
            />
            <StoryItem
              avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
              name="Haireus"
              time="3 giờ"
              newItem={2}
            />
            <StoryItem
              avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
              name="Haireus"
              time="3 giờ"
              newItem={2}
            />
            <StoryItem
              avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
              name="Haireus"
              time="3 giờ"
              newItem={2}
            />
            <StoryItem
              avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
              name="Haireus"
              time="3 giờ"
              newItem={2}
            />

            <StoryItem
              avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
              name="Haireus"
              time="3 giờ"
              newItem={2}
            />
            <StoryItem
              avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
              name="Haireus"
              time="3 giờ"
              newItem={2}
            />
            <StoryItem
              avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
              name="Haireus"
              time="3 giờ"
              newItem={2}
            />
          </div>
        </div>
      </div>

      <div className={styles.storyDetailRight}>
        <div className={styles.storyDetailRightWrapper}>
          <div className={styles.headerRight}>
            <HeaderRight />
          </div>

          <div className={styles.mainStory}>haireus</div>
        </div>
      </div>
    </div>
  );
};

interface StoryItemProps {
  avatar: string;
  name: string;
  time: string;
  newItem: number;
}
const StoryItem = ({ avatar, name, time, newItem }: StoryItemProps) => {
  return (
    <div className={styles.storyItem}>
      <div className={styles.avatar}>
        <img src={avatar} alt="" />
      </div>
      <div>
        <div>{name}</div>
        <div>
          {newItem ? (
            <>
              <span className={styles.newItem}>{`${newItem}`} thẻ mới</span>
              <span className="mr-5 ml-5">&#183;</span>
            </>
          ) : null}

          <span className={styles.time}>{time}</span>
        </div>
      </div>
    </div>
  );
};
