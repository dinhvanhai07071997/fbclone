import { IMAGE } from "common/const";
import icAdd from "assets/icons/add-white.svg";
import React from "react";
import styles from "./styles.module.scss";
import { Divider } from "antd";

import icVideo from "assets/icons/video-call.svg";
import icImage from "assets/icons/image.svg";
import icEmotion from "assets/icons/emotion.svg";
import icThreeDot from "assets/icons/threeDot.svg";
import icGlobal from "assets/icons/global.svg";
import { Story } from "./Story";
import ExpandableParaGraph from "components/Core/ExpandableParagraph";
import CommentSection from "components/CommentSection";
import ReactionSection from "components/ReactionSection";
import { REACTION } from "./data";

export const HomeContent = () => {
  return (
    <div className={styles.pageWrapper}>
      <Story />
      <NewPost />
      <MainContent />
    </div>
  );
};

const NewPost = () => {
  return (
    <div className={styles.newPostWrapper}>
      <div className={styles.newPostInput}>
        <img src={IMAGE} alt="" />
        <div>Jessica, What are you thinking?</div>
      </div>
      <Divider className="mt-15 mb-15" />
      <div className={styles.newPostAction}>
        <div>
          <img src={icVideo} alt="" /> Live streaming
        </div>
        <div>
          <img src={icImage} alt="" /> Photo/Video
        </div>
        <div>
          <img src={icEmotion} alt="" /> Feeling/Activity
        </div>
      </div>
    </div>
  );
};

interface IMainContent {}
const MainContent = ({}: IMainContent) => {
  return (
    <div className={styles.mainContent}>
      <div className="plr-20">
        <div className="pt-10">Gợi ý cho bạn</div>
        <Divider className="mt-10 mb-10" />
        <div className={styles.headerContent}>
          <div className={styles.headerRight}>
            <img className={styles.logo} src={IMAGE} alt="" />
            <div>
              <div>Jessica</div>
              <div>
                3h ago <img src={icGlobal} alt="" />
              </div>
            </div>
          </div>
          <div>
            <img src={icThreeDot} alt="" />
          </div>
        </div>

        <ExpandableParaGraph
          content="/ Có một kiểu con gái trông bề ngoài thì giống như có tính cách rất tốt, lễ phép, cũng dễ dàng tiếp xúc, nhưng tại sao lại khiến cho người ta có cảm giác xa cách?/
1. Quá chú trọng về ranh giới giữa người với người, và hầu hết đều muốn giữ một mối quan hệ ở mức độ xã giao.
2. Nghĩ rất nhiều, nội tâm phong phú, thế nhưng rất ít bộc lộ ra bên ngoài, ở nơi công cộng sẽ không nói nhiều, vì thế tạo cho người khác cảm giác lạnh nhạt.
3. Kì thật không phải người hướng nội, chỉ là thích một buổi trò chuyện hai người, nếu như có thêm người khác, cô ấy sẽ là người nói ít nhất trong số họ.
4. Quá lười, không muốn có thêm quá nhiều bạn, lười tạo dựng mối quan hệ sâu sắc, cô ấy thích cảm giác tự nhiên như nước hơn.
5. Không phải là một người giỏi ăn nói, khi cô ấy gặp chuyện buồn, sẽ không đem nỗi buồn ấy kể cho bất cứ ai. Kể cả khi tìm một người để nói chuyện cũng sẽ không nhắc đến chuyện đau lòng đó. Rất khó để tìm thấy một người đồng cảm với mình.
6. Tự nhiên bi quan về các mối quan hệ."
        />

        <div className={styles.contentMedia}>
          <img
            src="https://cdn.pixabay.com/photo/2020/06/01/22/23/eye-5248678__340.jpg"
            alt=""
            className={styles.img}
          />
        </div>

        <ReactionSection data={REACTION} />
        <Divider className="mt-5 mb-5" />
        <CommentSection />
      </div>
    </div>
  );
};
