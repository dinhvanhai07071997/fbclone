import { Divider } from "antd";
import React from "react";
import { useTranslation } from "react-i18next";
import icSearch from "assets/icons/search.svg";
import icCamera from "assets/icons/camera.svg";
import icThreeDot from "assets/icons/threeDot.svg";
import styles from "./styles.module.scss";

const listConversation = [
  {
    id: 1,
    name: "Đinh Văn Hải",
    avatar: "https://taimienphi.vn/tmp/cf/aut/anh-gai-xinh-1.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },
  {
    id: 2,
    name: "Huấn Hoa Hồng",
    avatar:
      "https://1.bigdata-vn.com/wp-content/uploads/2021/12/1640841264_102_Hinh-Nen-Girl-Xinh-Full-HD-Cho-Laptop-Va-May.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },
  {
    id: 3,
    name: "Rượu",
    avatar:
      "https://genk.mediacdn.vn/thumb_w/690/2019/12/26/photo-1-15773329973071527377344.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },

  {
    id: 4,
    name: "Haireus",
    avatar:
      "https://image-us.24h.com.vn/upload/2-2021/images/2021-05-31/anh-4-1622435533-350-width650height813.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },

  {
    id: 5,
    name: "Liam	Olivia",
    avatar:
      "https://kenh14cdn.com/2020/9/27/img3814-16008495660052057963035-16012244314321556076455.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },

  {
    id: 6,
    name: "Noah	Emma",
    avatar: "https://toplist.vn/images/800px/ha-vi-550877.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },

  {
    id: 7,
    name: "Oliver	Ava",
    avatar:
      "https://recmiennam.com/wp-content/uploads/2020/09/anh-gai-xinh-facebook-21.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },
  {
    id: 8,
    name: "William	Sophia",
    avatar:
      "https://pdp.edu.vn/wp-content/uploads/2021/06/hinh-anh-gai-xinh-de-thuong-nhat-1.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },

  {
    id: 9,
    name: "Benjamin	Isabella",
    avatar:
      "https://image-us.24h.com.vn/upload/2-2021/images/2021-05-31/anh-4-1622435533-350-width650height813.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },
  {
    id: 10,
    name: "Lucas",
    avatar: "https://luv.vn/wp-content/uploads/2021/08/hinh-anh-gai-xinh-1.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },
  {
    id: 11,
    name: "Evelyn",
    avatar:
      "https://i.vietgiaitri.com/2019/7/6/them-mot-girl-xinh-viet-len-bao-trung-cao-1m52-tron-tron-ma-de-thuong-2bfd49.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },
  {
    id: 12,
    name: "Aleander Harper",
    avatar:
      "https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },
  {
    id: 13,
    name: "Aleander Harper",
    avatar:
      "https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },
  {
    id: 14,
    name: "Aleander Harper",
    avatar:
      "https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg",
    infor: "",
    description: "Bạn chung với 5 người khác",
  },
];

export const ListMessage = () => {
  const { t } = useTranslation();
  return (
    <div>
      <div className="mt-20 font-size-16">Được tài trợ</div>
      <Divider className="mt-10 mb-5" />
      <div className="d-flex justify-content-between align-items-baseline">
        <div className="mt-20 font-size-16">Nguời liên hệ</div>
        <div className="mr-10">
          <img className="cursor-pointer mr-20" src={icCamera} alt="" />
          <img className="cursor-pointer mr-20" src={icSearch} alt="" />
          <img className="cursor-pointer mr-10" src={icThreeDot} alt="" />
        </div>
      </div>

      <div className="mt-5">
        {listConversation.map((el) => (
          <ChatItems id={el.id} avatar={el.avatar} name={el.name} />
        ))}
      </div>
    </div>
  );
};

interface IProps {
  avatar: string;
  name: string;
  id: number;
}
const ChatItems = ({ avatar, name, id }: IProps) => (
  <div className={styles.chatItem} key={id}>
    <div className={styles.onlineCircle}></div>
    <img src={avatar} alt="" />
    <span>{name}</span>
  </div>
);
