import React, { useState } from "react";
import { IMAGE } from "common/const";
import icAdd from "assets/icons/add-white.svg";
import styles from "./styles.module.scss";
import { useHistory } from "react-router-dom";
import { updateLocationSearchQuery } from "helper";
import { Modal } from "antd";

export const Story = () => {
  return (
    <div className={styles.storyWrapper}>
      <div className={styles.myStory}>
        <img className={styles.img} src={IMAGE} alt="" />
        <div className={styles.icAdd}>
          <img src={icAdd} alt="" />
        </div>
        <div className="mt-25">Tạo tin</div>
      </div>
      <StoryItem
        name="Hai Dinh"
        imageStory="https://hinhgaixinh.com/wp-content/uploads/2021/12/bo-anh-girl-xinh-cap-2.jpg"
        avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
      />
      <StoryItem
        name="Alex"
        imageStory="https://recmiennam.com/wp-content/uploads/2020/10/ngam-anh-gai-xinh-dep-dung-nghia-hot-girl-19.jpg"
        avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
      />
      <StoryItem
        name="Jessica"
        imageStory="https://kenh14cdn.com/2019/2/24/3561716420480213454575853861059020806684672n-15510057259571546306615.jpg"
        avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
      />
      <StoryItem
        name="Best"
        imageStory="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
        avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
      />

      <StoryItem
        name="Elizabeth"
        imageStory="https://i.pinimg.com/550x/cf/f3/6e/cff36edb1f8925ec1b9a36c0e1ea814f.jpg"
        avatar="https://hinhgaixinh.com/wp-content/uploads/2021/10/anh-co-gai-xinh-dep-tuyet-nhung.jpg"
      />
    </div>
  );
};

interface IProps {
  name: string;
  imageStory: string;
  avatar: string;
}
const StoryItem = ({ name, imageStory, avatar }: IProps) => {
  const history = useHistory();
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    history.push(`/stories?id=${name}&source=story-home`);
  };

  const handleCancel = () => {
    // window.location.pathname = ``;
  };
  return (
    <div className={styles.storyItem} onClick={handleClick}>
      <img src={imageStory} alt="" />
      <div className={styles.name}>{name}</div>
      <div className={styles.layer}></div>

      {/* <Modal title="Basic Modal" visible={open} onCancel={() => handleCancel()}>
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Modal> */}
    </div>
  );
};
