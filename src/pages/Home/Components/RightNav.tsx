import { IMAGE } from "common/const";
import React from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import friendImg from "assets/images/friends.png";
import groupImg from "assets/images/group.png";
import watchImg from "assets/images/watch.png";
import memoryImg from "assets/images/memory.png";
import marketImg from "assets/images/market-place.png";
import healthImg from "assets/images/health.png";
import messageImg from "assets/images/messenger.png";
import adManageImg from "assets/images/adManager.png";
import styles from "./styles.module.scss";
import { Divider } from "antd";

export const RightNav = () => {
  const { t } = useTranslation();
  return (
    <div className={styles.rightNavWrapper}>
      <Items image={IMAGE} link={"/profile"} title={t("rightNav.profile")} />
      <Items
        image={friendImg}
        link={"/friends"}
        title={t("rightNav.friends")}
      />
      <Items image={groupImg} link={"/profile"} title={t("rightNav.group")} />
      <Items
        image={watchImg}
        link={"/profile"}
        title={t("rightNav.watch")}
        isNew={true}
      />
      <Items
        image={marketImg}
        link={"/profile"}
        title={t("rightNav.marketPlace")}
      />
      <Items image={memoryImg} link={"/profile"} title={t("rightNav.memory")} />
      <Items
        image={messageImg}
        link={"/profile"}
        title={t("rightNav.message")}
      />
      <Items image={healthImg} link={"/profile"} title={t("rightNav.health")} />
      <Items
        image={adManageImg}
        link={"/profile"}
        title={t("rightNav.adManage")}
      />
      <Divider />

      <div className={styles.shortCut}>{t("rightNav.your_shortcut")}</div>
      <Items
        image={messageImg}
        link={"/profile"}
        title={t("rightNav.health")}
      />
    </div>
  );
};

interface ItemProps {
  image: any;
  link: string;
  title: string;
  isNew?: boolean;
}
const Items = ({ image, link, title, isNew = false }: ItemProps) => {
  const history = useHistory();
  const handleClick = () => {
    history.push(link);
  };
  return (
    <div className={styles.rightNavItem} onClick={handleClick}>
      <img src={image} alt="" />
      <div>
        <div>{title}</div>
        {isNew && <div className={styles.textNew}>● 9+ new</div>}
      </div>
    </div>
  );
};
