export interface IProps {}

export interface IProfile {
  id: number;
  address: string;
  avatar: string;
  banner: string;
  bio: string;
  dob: string;
  email: string;
  firstName: string;
  gender: number;
  isOnline: number;
  lastName: string;
  lastOnline: string;
  marrageStatus: number;
  status: number;
}
