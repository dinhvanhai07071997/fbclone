export const PAGESIZE = 20;
export const BIG_PAGESIZE = 10000;
export const MIN_PASSWORD = 6;
export const MAX_PASSWORD = 32;
export const MAX_LENGTH = 255;
export const MAX_PERCENT = 100;
export const MIN_PERCENT = 0;
export const MAX_LENGTH_CHARTER_CAPITAL = 19;
export const PATTERN_EMAIL =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const PATTERN_PHONE = /^[+]*[0-9]{10,12}$/;
export const PATTERN_KATAKANA = /^[ァ-ン、　 ー]+$/;

export const COMMON_GUTTER = 30;
export const COL_SPAN_1 = 7;
export const COL_SPAN_2 = 24 - COL_SPAN_1 * 2;
export const IMAGE = `https://www.dungplus.com/wp-content/uploads/2019/12/girl-xinh-1-480x600.jpg`;

export const languages = [
  {
    code: "en",
    name: "English",
  },
  {
    code: "vi",
    name: "Tiếng Việt",
  },
];

export const listDayOfMonth = Array.from({ length: 31 }, (_, i) =>
  i < 10 ? `0${i}` : `${i + 1}`
);
export const listMonthOfYear = Array.from({ length: 12 }, (_, i) =>
  i < 10 ? `0${i}` : `${i + 1}`
);
export const listYear = Array.from(
  { length: 2022 - 1900 },
  (_, i) => i + 1900
).reverse();
