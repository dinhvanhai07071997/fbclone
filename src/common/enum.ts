export enum CommonStatus {
  IN_ACTIVE = 0,
  ACTIVE = 1,
  VERIFY = 2,
}

export enum MarrageStatus {
  SINGLE = 1,
  DATING = 2,
  MARRAGE = 3,
}

export enum UserGender {
  MALE = 1,
  FEMALE = 2,
  OTHER = 3,
}

export enum OtpType {
  REGISTER = 1,
  CHANGE_PASSWORD = 2,
  FORGET_PASSWORD = 3,
}

export enum ResourceType {
  TERM = 1,
  POLICY = 2,
  HELP = 3,
}

export enum RelationShipType {
  FOLLOWING = 1,
  FRIEND = 2,
  BLOCK = 3,
  CLOSE_FRIEND = 4,
}

export enum MediaType {
  IMAGE = 1,
  AUDIO = 2,
  VIDEO = 3,
  PDF = 4,
  STICKER = 5,
}

export enum ReactionType {
  LIKE = 1,
  LOVE = 2,
  CARE = 3,
  HAHA = 4,
  SAD = 5,
  ANGRY = 6,
}

export enum ImageUsage {
  AVATAR = 1,
  BANNER = 2,
  POST = 3,
  STORY = 4,
  MESSAGE = 5,
}

export enum NotificationTargetType {
  COMMON = 1,
  ALL = 2,
}
export enum NotificationType {
  COMMON = 1,
  ADMIN_SEND = 2,
}

export enum ReadNotification {
  UNREAD = 0,
  READ = 1,
}

export enum NotificationRedirectType {
  HOME = 1,
  POST_DETAIL = 2,
  PROFILE_MEMBER = 3,
  NOTIFICATION = 4,
}
export enum RedirectType {
  NOTIFICATION = 1,
}

export enum KeyQueue {
  PUSH_NOTIFICATION = "PUSH_NOTIFICATION",
  ADMIN_PUSH_NOTIFICATION = "ADMIN_PUSH_NOTIFICATION",
  SEND_MAIL = "SEND_MAIL",
}

export enum VerificationCodeStatus {
  ACTIVE = 1,
  USED = 2,
  INACTIVE = 0,
}

export enum SearchType {
  USER = 1,
  PAGE = 2,
  GROUP = 3,
}
