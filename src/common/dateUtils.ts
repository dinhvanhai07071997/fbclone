import moment from 'moment';

const formatFullTime = 'YYYY/MM/DD HH:mm';
const formatDate = 'YYYY/MM/DD';
const formatDatabaseDate = 'YYYY-MM-DD';
const formatHour = 'HH:mm';

export const dateUtils = {
  startOf: (date: any) => {
    return moment(date).startOf('day').toISOString();
  },
  endOf: (date: any) => {
    return moment(date).endOf('day').toISOString();
  },
  getDateOfJapan: (date: any) => {
    return moment(date).format('M月D日');
  },
  getDateToString: (date: any) => {
    if (!date) return undefined;
    return moment(date).format('YYYY-MM-DD');
  },
  getToday: () => moment(),
  currentFullTime: () => {
    return moment().format(formatFullTime);
  },
  currentFullTimeJapan: () => {
    return moment().format('YYYY年-MM月-DD日');
  },

  currentFullTimeJapan2: () => {
    return moment().format('YYYY年M月D日');
  },
  getFullTimeJapan: (date: any) => {
    return moment(date).format('YYYY年M月D日');
  },
  currentDate: () => {
    return moment().format(formatDate);
  },
  currentDatabaseDate: (date: any) => {
    return moment(date).format(formatDatabaseDate);
  },
  currentHour: () => {
    return moment().format(formatHour);
  },

  getFullTime: (date: string) => {
    return moment(date).format(formatFullTime);
  },

  getDate: (date: string | undefined) => {
    if (!date) return '';
    return moment(date).format(formatDate);
  },

  getYear: (date: any) => {
    return moment(date).format('YYYY');
  },

  getHour: (date: any) => {
    return moment(date).format(formatHour);
  },

  convertTime: (time: string) => {
    return moment(time, 'HH:mm');
  },

  isAfter: (startTime: any, endTime: any) => {
    return endTime.isAfter(startTime);
  },
  startOfWeekDatabase: (date: any) => {
    return moment(date).startOf('week').format(formatDatabaseDate);
  },
  endOfWeekDatabase: (date: any) => {
    return moment(date).endOf('week').format(formatDatabaseDate);
  },
  startOfWeekDisplay: (date: any) => {
    return moment(date).startOf('week').format(formatDate);
  },
  endOfWeekDisplay: (date: any) => {
    return moment(date).endOf('week').format(formatDate);
  },
  getNextWeek: () => {
    return moment().add(1, 'week');
  },
  disableCurrentWeek: (current: any) => {
    return current && current < moment().add(1, 'week').startOf('week');
  },
  disablePreviousWeek: (current: any) => {
    return current && current < moment().startOf('week');
  },
  startOfWeek: (date: any) => {
    return moment(date).startOf('week');
  },
  getDay: (date: any) => {
    return moment(date);
  },

  getDayIndex: (date: any) => {
    return moment(date).format('DD');
  },

  getWeekday: () => {
    return moment().weekday();
  },
  getIsoWeekday: () => {
    return moment().isoWeekday();
  },
  getEndOfMonth: (date: any) => {
    return moment(date).endOf('month');
  },
  getStartOfMonth: (date: any) => {
    return moment(date).startOf('month');
  },
  getSalaryPaymentDate: (date: any, paymentDate: number) => {
    return moment(date)
      .startOf('month')
      .add(paymentDate - 1, 'd')
      .format('YYYY/MM/DD');
  },
  getDayInMonth: (date: any) => {
    return moment(date).daysInMonth();
  },
  getDateOfMonth: (index: number, date: any) => {
    return moment(date).date(index);
  },
  getWeeksDay: (date: any) => {
    return moment(date).weekday();
  },
  isBeforeTime: (start: any, end: any) => {
    const startTime = typeof start === 'string' ? start : moment(new Date(start)).format(formatHour);
    const endTime = typeof end === 'string' ? end : moment(new Date(end)).format(formatHour);

    if (startTime.toString() <= endTime.toString()) {
      return true;
    }
    return false;
  },
  
  isBetweenTime: (startTime: any, endTime: any, value: any) => {
    const start = typeof startTime === 'string' ? startTime : moment(new Date(startTime)).format(formatHour);
    const end = typeof endTime === 'string' ? endTime : moment(new Date(endTime)).format(formatHour);
    const currentValue = moment(new Date(value)).format(formatHour);

    if (start.toString() <= currentValue.toString() && currentValue.toString() <= end.toString()) return true;
    return false;
  },

  getPreviousMonth: (date: any) => {
    return moment(date).subtract(1, 'months');
  },
  getNextMonth: (date: any) => {
    return moment(date).add(1, 'months');
  },
  getDifferentHours : (startDate:any , endDate: any ) =>  moment(endDate).diff(moment(startDate), 'hours'),
};
